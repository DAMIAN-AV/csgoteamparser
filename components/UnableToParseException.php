<?php

namespace app\components;

use yii\base\UserException;

class UnableToParseException extends UserException
{

}