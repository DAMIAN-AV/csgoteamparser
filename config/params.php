<?php

return [
    'adminEmail' => 'admin@example.com',
    'domainForParsing' => "https://www.hltv.org",
    'selectors' => [
        'teamSelector' => "body > div.bgPadding > div > div.colCon > div.leftCol > div > div:nth-child(1) > div.sidebar-first-level > div > div > span",
        'playersLinkSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div.gtSmartphone-only > div > div > a:nth-child(4)",
        'overviewSelectors' => [
            'mapsPlayedSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(5) > div:nth-child(1) > div.large-strong",
            'winsDrawsLossesSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(5) > div:nth-child(2) > div.large-strong",
            'totalKillsSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(5) > div:nth-child(3) > div.large-strong",
            'totalDeathsSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(7) > div:nth-child(1) > div.large-strong",
            'roundsPlayedSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(7) > div:nth-child(2) > div.large-strong",
            'kDRatioSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > div:nth-child(7) > div:nth-child(3) > div.large-strong",
        ],
        'playersSelector' => "body > div.bgPadding > div > div.colCon > div.contentCol > div > table > tbody > tr",
    ],
];
