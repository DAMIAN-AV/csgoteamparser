<?php

namespace app\modules\api\controllers;

use app\models\Player;
use app\models\Overview;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class TeamController extends ActiveController
{
    /*
     *
     * The class "TeamController" is responsible for providing information about the team.
     *
     */

    /**
     * @var string
     */
    public $modelClass = "app\\models\\Overview";

    /**
     *
     * Disable all parent actions
     *
     * @return array|null
     */
    public function actions()
    {
        return null;
    }

    /**
     *
     * Return team information
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGet()
    {
        $name = \Yii::$app->request->getQueryParam("name");

        $overview = Overview::find()->select([
                'maps_played',
                'wins_draws_losses',
                'total_kills',
                'total_deaths',
                'rounds_played',
                'k_d_ratio']
        )->where(['team' => $name])->limit(1)->one();
        if (!$overview) {
            throw new NotFoundHttpException("Overview not found");
        }
        unset($overview->id);

        $players = Player::find()->select([
            'player',
            'k_d',
            'k_d_diff'
        ])->where(['team' => $name])->all();
        if (!$players) {
            throw new NotFoundHttpException("Players not found");
        }


        $response = [
            'overview' => $overview,
            'players' => $players,
        ];

        return $response;
    }

}
