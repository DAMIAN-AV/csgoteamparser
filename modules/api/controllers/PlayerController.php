<?php

namespace app\modules\api\controllers;

use app\models\Player;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class PlayerController extends ActiveController
{
    /*
     *
     * The class "PlayerController" is responsible for providing information about player
     *
     */

    /**
     * @var string
     */
    public $modelClass = "app\\models\\player";

    /**
     *
     * Disable all parent actions
     *
     * @return array|null
     */
    public function actions()
    {
        return null;
    }


    public function actionGet()
    {
        $name = \Yii::$app->request->getQueryParam("name");

        $player = Player::find()->select([
            'player',
            'k_d',
            'k_d_diff'
        ])->where(['player' => $name])->limit(1)->one();
        if (!$player) {
            throw new NotFoundHttpException("Player not found");
        }


        return $player;
    }

}
