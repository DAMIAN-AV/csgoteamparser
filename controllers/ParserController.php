<?php
/**
 * Created by PhpStorm.
 * User: North
 * Date: 07.02.2019
 * Time: 8:32
 */

namespace app\controllers;


use app\models\Parser;
use yii\web\Controller;

class ParserController extends Controller
{
    /**
     * @return string
     */
    public function actionAdd()
    {
        $parser = new Parser();

        if ($parser->load(\Yii::$app->request->post()) && $parser->validate()) {

            $parser->save();

            return $this->render('add-confirm', ['model' => $parser]);
        } else {
            return $this->render('add', ['model' => $parser]);
        }
    }

    /**
     * @param null $id
     * @return string
     * @throws \app\components\UnableToParseException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionStart($id = null)
    {
        $team = Parser::start($id);

        return $this->render("complete", ['team' => $team]);


    }

}