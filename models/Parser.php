<?php

namespace app\models;

use app\components\UnableToParseException;
use phpQuery;
use Yii;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "parser".
 *
 * @property int $id
 * @property string $url
 */
class Parser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string'],
            ['url', 'required', 'message' => "Url is empty"],
            ['url', 'unique', 'message' => "This url is already exists"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
        ];
    }

    /**
     * @param $url
     * @return array
     * @throws UnableToParseException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    private static function parsePlayers($url)
    {
        /**
         * Get html from website
         */
        $client = new Client(['baseUrl' => $url]);
        $html = $client->createRequest()->send();

        /**
         * Load html to phpQuery
         */
        phpQuery::newDocumentHTML($html);

        /**
         * Parse players data
         */
        $selectors = \Yii::$app->params['selectors'];

        $playersHtml = pq($selectors['playersSelector']);
        $players = [];

        for ($i = 0; $i < $playersHtml->length; $i++) {
            $playerHtml = $playersHtml->eq($i);
            $player = [
                'team' => self::getTeamNameFormUrl($url),
                'player' => $playerHtml->find("td > a")->text(),
                'maps' => $playerHtml->find("td:nth-child(2)")->text(),
                'k_d_diff' => $playerHtml->find("td:nth-child(3)")->text(),
                'k_d' => $playerHtml->find("td:nth-child(4)")->text(),
                'rating' => $playerHtml->find("td:nth-child(5)")->text(),
            ];
            array_push($players, $player);
        }

        /**
         * Validate parsed data
         */
        if (empty($players)) {
            throw new UnableToParseException("Unable to parse players. No players found.");
        }
        foreach ($player as $item) {
            if (empty($item)) {
                throw new UnableToParseException("Unable to parse players. Players data is missing.");
            }
        }

        return $players;
    }

    /**
     * @param $url
     * @return array
     * @throws UnableToParseException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    private static function parseOverview($url)
    {
        /**
         * Get html from website
         */
        $client = new Client(['baseUrl' => $url]);
        $html = $client->createRequest()->send();

        /**
         * Load html to phpQuery
         */
        phpQuery::newDocumentHTML($html);

        /**
         * Parse data
         */
        $selectors = Yii::$app->params['selectors'];
        $overviewSelectors = $selectors['overviewSelectors'];

        $overview = [
            'team' => self::getTeamNameFormUrl($url),
            'wins_draws_losses' => pq($overviewSelectors['winsDrawsLossesSelector'])->text(),
            'maps_played' => pq($overviewSelectors['mapsPlayedSelector'])->text(),
            'total_kills' => pq($overviewSelectors['totalKillsSelector'])->text(),
            'total_deaths' => pq($overviewSelectors['totalDeathsSelector'])->text(),
            'rounds_played' => pq($overviewSelectors['roundsPlayedSelector'])->text(),
            'k_d_ratio' => pq($overviewSelectors['kDRatioSelector'])->text(),
        ];

        /**
         * Validate parsed data
         */
        foreach ($overview as $item) {
            if (empty($item)) {
                throw new UnableToParseException("Unable to parse team. Overview data is missing.");
            }
        }

        return $overview;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws UnableToParseException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function start($id)
    {

        /**
         * Find parser
         */
        $parser = Parser::findOne($id);

        if (!$parser) {
            throw new NotFoundHttpException("Parser not found.");
        }
        $url = $parser->url;
        $playersUrl = str_replace("/stats/teams/", "/stats/teams/players/", $url);

        /**
         * Parse data
         */
        $team['name'] = self::getTeamNameFormUrl($url);

        $overview = self::parseOverview($url);
        $players = self::parsePlayers($playersUrl);

        $team['overview'] = $overview;
        $team['players'] = $players;

        /**
         * Save parsed data
         */
        $overviewModel = Overview::findOne(['team' => $team['name']]);
        if (!$overviewModel) {
            $overviewModel = new Overview();
        }
        $overviewModel->attributes = $overview;
        if (!$overviewModel->save()) {
            return $overviewModel->getErrors();
        }

        Player::deleteAll(['team' => $team['name']]);

        foreach ($players as $player) {
            $playerModel = new Player();
            $playerModel->attributes = $player;
            $playerModel->save();
            if (!$playerModel->save()) {
                return $playerModel->getErrors();
            }
        }

        return $team;
    }

    /**
     * @param $url
     * @return mixed
     */
    private static function getTeamNameFormUrl($url)
    {
        /**
         * Decode and remove GET params from url if they exist
         */
        $url = urldecode(((strpos($url, "?") === false) ? $url : substr($url, 0, strpos($url, "?"))));
        /**
         * Cut and return team name from url
         */
        return empty(explode("/", $url)[substr_count($url, "/")]) ? explode("/", $url)[substr_count($url, "/") - 1] : explode("/", $url)[substr_count($url, "/")];
    }
}
