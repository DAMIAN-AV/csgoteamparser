<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string $team
 * @property string $player
 * @property int $maps
 * @property double $k_d_diff
 * @property double $k_d
 * @property double $rating
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team', 'player', 'maps', 'k_d_diff', 'k_d', 'rating'], 'required'],
            [['team', 'player'], 'string'],
            [['maps'], 'integer'],
            [['k_d_diff', 'k_d', 'rating'], 'number'],
            ['player', 'unique', 'message' => 'Player already exists']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team' => 'Overview',
            'player' => 'Player',
            'maps' => 'Maps',
            'k_d_diff' => 'K D Diff',
            'k_d' => 'K D',
            'rating' => 'Rating',
        ];
    }
}
