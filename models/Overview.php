<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "overview".
 *
 * @property int $id
 * @property string $team
 * @property int $maps_played
 * @property string $wins_draws_losses
 * @property int $total_kills
 * @property int $total_deaths
 * @property int $rounds_played
 * @property double $k_d_ratio
 */
class Overview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'overview';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team', 'maps_played', 'wins_draws_losses', 'total_kills', 'total_deaths', 'rounds_played', 'k_d_ratio'], 'required'],
            [['team', 'wins_draws_losses'], 'string'],
            [['maps_played', 'total_kills', 'total_deaths', 'rounds_played'], 'integer'],
            [['k_d_ratio'], 'number'],
            ['team', 'unique', 'message' => 'Overview already exists'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team' => 'Overview',
            'maps_played' => 'Maps Played',
            'wins_draws_losses' => 'Wins Draws Losses',
            'total_kills' => 'Total Kills',
            'total_deaths' => 'Total Deaths',
            'rounds_played' => 'Rounds Played',
            'k_d_ratio' => 'K D Ratio',
        ];
    }
}
